<?php

return [
    'user_front_end_route_name' => 'users.lists',
    'role_front_end_route_name' => 'users.lists',
    'settings_front_end_route_name' => 'core.settings.index',
    'pipeline_front_end_route_name' => 'deals_pipeline.page',
    'deal_front_end_route_name' => 'deals.lists',


    /*
     |
     | Check if the notification has to get the audiences from application
     |
     */
    'default_audiences' => true,
];
