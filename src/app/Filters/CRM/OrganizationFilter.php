<?php


namespace App\Filters\CRM;


use App\Filters\Core\traits\CreatedByFilter;
use App\Filters\CRM\Traits\ContactTypeFilterTrait;
use App\Filters\CRM\Traits\DateFilterTrait;
use App\Filters\CRM\Traits\OwnerFilterTrait;
use App\Filters\CRM\Traits\TagsFilterTrait;
use Illuminate\Database\Eloquent\Builder;

class OrganizationFilter extends UserActivityFilter
{
    use CreatedByFilter,
        ContactTypeFilterTrait,
        OwnerFilterTrait,
        TagsFilterTrait,
        DateFilterTrait;

    public function person($ids = null)
    {
        $persons = explode(',', $ids);

        $this->builder->when($ids, function (Builder $query) use ($persons) {
            $query->whereHas('persons', function (Builder $query) use ($persons) {
                $query->whereIn('person_id', $persons);
            });
        });


    }

    public function search($search = null)
    {
        return $this->builder->when($search, function (Builder $builder) use ($search) {
            $builder->where('name', 'LIKE', "%$search%")
                ->orWhere('address', 'LIKE', "%{$search}%");
        });
    }
}
