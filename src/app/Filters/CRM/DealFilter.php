<?php


namespace App\Filters\CRM;


use App\Filters\CRM\Traits\DateFilterTrait;
use App\Filters\CRM\Traits\DealValueFilter;
use App\Filters\CRM\Traits\DealWithProposalsFilterTrait;
use App\Filters\CRM\Traits\OwnerFilterTrait;
use App\Filters\CRM\Traits\StatusFilterTrait;
use App\Filters\CRM\Traits\TagsFilterTrait;
use Illuminate\Database\Eloquent\Builder;

class DealFilter extends UserActivityFilter
{

    use OwnerFilterTrait,
        DateFilterTrait,
        DealValueFilter,
        DealWithProposalsFilterTrait,
        TagsFilterTrait,
        StatusFilterTrait;

    public function context($type)
    {
        // person
        if ($type == 'person') {
            return $this->builder->where(function (Builder $query) {
                $query->where('contextable_type', 'App\\Models\\CRM\\Person\\Person');
            });
        }

        // organization
        if ($type == 'organization') {
            return $this->builder->where(function (Builder $query) {
                $query->where('contextable_type', 'App\\Models\\CRM\\Organization\\Organization');
            });
        }
    }
    public function pipeline($ids = null)
    {
        if ($ids)
        {
            $pipeline_id = explode(',', $ids);
            return $this->builder->whereIn('pipeline_id', $pipeline_id);
        }
        return $this->builder;
    }

    public function contactType($ids = null)
    {
        $contact_type = explode(',', $ids);

        return $this->builder->when($ids, function (Builder $builder) use ($contact_type) {

            $builder->whereHas('contactPerson', function (Builder $builder) use ($contact_type) {
                $builder->whereIn('contact_type_id', $contact_type);
            });

        });
    }

    public function search($search = null)
    {
        return $this->builder->when($search, function (Builder $builder) use ($search) {
            $builder->where('title', 'LIKE', "%$search%");
        });
    }
}
