<?php

namespace App\Http\Controllers\CRM\Country;

use App\Http\Controllers\Controller;
use App\Services\CRM\Country\CountryService;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function __construct(CountryService $countryService)
    {
        $this->service = $countryService;
    }
    public function index()
    {
        return $this->service->get();
    }
}
