<?php

namespace App\Http\Controllers\CRM\Activity;

use App\Http\Controllers\Controller;
use App\Http\Requests\CRM\Activity\ActivityTypeRequest;
use App\Models\CRM\Activity\ActivityType;
use App\Services\CRM\Activity\ActivityTypeService;

class ActivityTypeController extends Controller
{
    public function __construct(ActivityTypeService $activityTypeService)
    {
        $this->service = $activityTypeService;
    }


    public function index()
    {
        return $this->service
            ->select(['id', 'name'])
            ->paginate(15);
    }


    public function create()
    {
        //
    }


    public function store(ActivityTypeRequest $request)
    {
        $activity_type = $this->service->save();

        return created_responses('activity_type', [
            'activity_type' => $activity_type
        ]);
    }


    public function show($id)
    {
        return $this->service->showActivityTypeDetails($id);
    }


    public function edit($id)
    {
        //
    }


    public function update(ActivityTypeRequest $request, ActivityType $activityType)
    {
        $activity_type = $activityType->update($request->all());

        return updated_responses('activity_type', [
            'activity_type' => $activityType
        ]);
    }


    public function destroy(ActivityType $activityType)
    {
        $activityType->delete();

        return deleted_responses('activity_type');
    }
}
