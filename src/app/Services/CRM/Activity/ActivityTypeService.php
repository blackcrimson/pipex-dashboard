<?php


namespace App\Services\CRM\Activity;


use App\Models\CRM\Activity\ActivityType;
use App\Services\ApplicationBaseService;

class ActivityTypeService extends ApplicationBaseService
{
    public function __construct(ActivityType $activityType)
    {
        $this->model = $activityType;
    }

    public function showActivityTypeDetails($id)
    {
        $relational_data = [
            'activity.status:id,name,class,type',
            'activity.CreatedBy:id,first_name,last_name'
        ];

        return $this->model
            ->where('id', $id)
            ->with($relational_data)
            ->first();
    }
}
