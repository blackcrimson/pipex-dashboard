<?php

namespace App\Services\CRM\Contact;

use App\Helpers\Core\Traits\FileHandler;
use App\Services\ApplicationBaseService;
use App\Helpers\CRM\Traits\StoreFileTrait;
use App\Models\CRM\Organization\Organization;
use App\Services\CRM\Traits\personOrganizationDetails;

class OrganizationService extends ApplicationBaseService
{
    use personOrganizationDetails, FileHandler, StoreFileTrait;

    public function __construct(Organization $organization)
    {
        $this->model = $organization;
    }

    public function showAll()
    {
        return $this->with([
            'contactType:id,name,class',
            'owner:id,first_name,last_name',
            'CreatedBy:id,first_name,last_name',
            'tags:id,name,color_code',
            'customFields',
            'persons',
            'country'
            // 'email.type',
            // 'phone.type'
        ]);
    }

    public function showOrganization($id, $pivot_data)
    {
        return $this->personOrganizationDetails($this->model, $id, $pivot_data);
    }

    public function fileSync($path, $organization)
    {
        foreach ($path as $key => $value) {
            $file_path = $this->fileStore(
                $value,
                'organization'
            );
            $file = $organization->files()->create([
                'type' => 'organization',
                'path' => $file_path,
            ]);

            //            $organization->activity()->create([
            //                'file' => $file->id,
            //            ]);
        }
    }
}
