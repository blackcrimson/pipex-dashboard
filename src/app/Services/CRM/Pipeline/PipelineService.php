<?php


namespace App\Services\CRM\Pipeline;


use App\Models\CRM\Pipeline\Pipeline;
use App\Services\ApplicationBaseService;

class PipelineService extends ApplicationBaseService
{
    public function __construct(Pipeline $pipeline)
    {
        $this->model = $pipeline;
    }
}
