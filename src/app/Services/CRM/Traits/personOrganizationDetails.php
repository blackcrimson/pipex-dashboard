<?php

namespace App\Services\CRM\Traits;

use Illuminate\Database\Eloquent\Model;

trait personOrganizationDetails
{
    use PersonOrganizationRelations;

    public function personOrganizationDetails(Model $model, $id, $pivot_data)
    {
        return $model->where('id', $id)
            ->with($this->relations($pivot_data))
            ->with(['customFields'])
            ->firstOrFail();
    }
}
