<?php

namespace App\Services\CRM\Traits;

use Illuminate\Database\Eloquent\Model;

trait QuickviewModalNavigationTrait
{
    use PersonOrganizationRelations;

    public function next(Model $model, $id, $pivot_data)
    {
        $instance = $model::query()->where('id', '>', $id)
            ->with($this->relations($pivot_data))
            ->first();

        $noMoreNext = $model::query()->where('id', '>', $instance->id)->first() === null;

        $noMorePrevious = $model::query()->where('id', '<', $instance->id)->first() === null;

        return [
            'noMoreNext' => $noMoreNext,
            'noMorePrevious' => $noMorePrevious,
            'current' => $instance
        ];
    }

    public function previous(Model $model, $id, $pivot_data)
    {
        $instance = $model::query()
            ->where('id', '<', $id)
            ->with($this->relations($pivot_data))
            ->get()->last();

        $noMoreNext = $model::query()->where('id', '>', $instance->id)->first() === null;

        $noMorePrevious = $model::query()->where('id', '<', $instance->id)->first() === null;

        return [
            'noMorePrevious' => $noMorePrevious,
            'noMoreNext' => $noMoreNext,
            'current' => $instance
        ];
    }
}
