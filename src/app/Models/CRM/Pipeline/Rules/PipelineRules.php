<?php


namespace App\Models\CRM\Pipeline\Rules;


trait PipelineRules
{
    public function createdRules()
    {
        return [
            'name' => 'required|max:255|unique:pipelines,name',
        ];
    }

    public function updatedRules()
    {
        return [
            'name' => 'required|max:255|unique:pipelines,name,'.request()->get('id'),
        ];
    }

}
