<?php

namespace App\Models\CRM\Pipeline\Traits;

trait PipelineAdditionalDataTrait
{
    /*
     * Get total stages
     */
    public function getTotalStagesAttribute()
    {
        return (int) $this->stage()->count();
    }

    /*
     * Get total deals
     */
    public function getTotalDealsAttribute()
    {
        return (int) $this->deals()->count();
    }

    /*
     * Get the summation of deal values
     */
    public function getTotalDealValueAttribute()
    {
        return (int) $this->deals()->sum('value');
    }
}
