<?php


namespace App\Models\CRM\Activity\Traits\Rules;


trait ActivityRules
{
    public function createdRules()
    {
        return [
            'activity_type_id' => 'required',
            'title' => 'required',
            'contextable_type' => 'required',
            'contextable_id' => 'required'
        ];
    }

    public function updatedRules()
    {
        return [
            'title' => 'required',
        ];
    }

}
