<?php

namespace App\Models\CRM\Deal;

use App\Models\Core\Traits\BootTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Core\BaseModel;
use App\Models\Core\File\File;
use App\Models\CRM\Organization\Organization;
use App\Models\CRM\Deal\Traits\DealRulesTrait;
use App\Models\Core\Traits\DescriptionGeneratorTrait;
use App\Models\CRM\Deal\Traits\DealRelationshipTrait;

class Deal extends BaseModel
{
    use BootTrait{
        boot as traitBoot;
    }
    use DealRelationshipTrait,              // All Relations are defined here
        DealRulesTrait,                     // Validation Rules
        DescriptionGeneratorTrait,
        HasFactory;

    protected $fillable = [
        'title',
        'value',
        'pipeline_id',
        'stage_id',
        'lost_reason_id',
        'status_id',
        'created_by',
        'owner_id',
        'contextable_type',
        'contextable_id',
        'expired_at',
        'histories',
        'comment',
    ];

    protected $casts = [
        'pipeline_id' => 'int',
        'stage_id' => 'int',
        'lost_reason_id' => 'int',
        'status_id' => 'int',
        'owner_id' => 'int',
        'person_id' => 'int',
        'organization_id' => 'int',
        'value' => 'int',
    ];

    protected $appends = [
        'total_followers',          // Returning total followers for Deal Details page
        'total_participants',       // Returning total participants for Deal Details page
        'next_activity',            // Returning next activity
        'avg_age_of_deal'
    ];

    protected static $logAttributes = [
        'title', 'value', 'pipeline', 'stage', 'status.name', 'person', 'organization', 'owner',
    ];

    public function organizations()
    {
        return $this->belongsToMany(Organization::class, 'organization_id');
    }

    public function fileUpload()
    {
        return $this->morphMany(File::class, 'fileable');
    }
    public static function boot()
    {
        parent::boot();

        self::traitBoot();

        static::deleting(function (self $deal) {
            $deal->activity()->delete();
        });
    }
}
