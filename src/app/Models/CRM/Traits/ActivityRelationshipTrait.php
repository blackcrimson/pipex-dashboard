<?php

namespace App\Models\CRM\Traits;

use App\Models\CRM\Activity\Activity;

trait ActivityRelationshipTrait
{
    public function activity()
    {
        return $this->morphMany(Activity::class, 'contextable');
    }

}
