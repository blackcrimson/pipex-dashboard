@extends('layouts.crm')
@section('title', 'Deals | Details')
@section('contents')
    <deal-details @if(isset($id)) selected-url="crm/deals/{{ $id }}" @endif/>
@endsection
