@extends('layouts.crm')
@section('title', 'Deals | Add/Edit Pipeline')
@section('contents')
    <app-edit-pipeline @if(isset($id)) selected-url="crm/pipelines/{{ $id }}" @endif/>
@endsection
