@extends('layouts.crm')
@section('title', 'Organization view')
@section('contents')
    <app-organization-view @if(isset($id)) selected-url="crm/organizations/{{ $id }}" @endif/>
@endsection
