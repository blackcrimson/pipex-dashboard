@extends('layouts.crm')
@section('title', 'Contacts')
@section('contents')
    <app-contact-person-details @if(isset($id)) selected-url="crm/persons/{{ $id }}" @endif/>
@endsection