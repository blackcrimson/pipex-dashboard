@extends('layouts.crm')
@section('title', 'Proposals')
@section('contents')
    <app-send-proposals @if(isset($id)) selected-url="crm/proposals/{{ $id }}" @endif
                        @if(isset($action)) action="{{$action}}" @endif>
    </app-send-proposals>
@endsection