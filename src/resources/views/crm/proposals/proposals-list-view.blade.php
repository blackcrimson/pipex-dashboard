@extends('layouts.crm')
@section('title', 'Proposals')
@section('contents')
    <app-proposals-list-view></app-proposals-list-view>
@endsection