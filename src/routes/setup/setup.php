<?php

use App\Http\Controllers\Setup\EnvironmentController;

Route::get('environment', [EnvironmentController::class, 'index'])
    ->name('environment.index');

Route::post('environment', [EnvironmentController::class, 'store'])
    ->name('environment.store');

Route::get('admin-info', [EnvironmentController::class, 'show'])
    ->name('environment.admin-info');

Route::post('install', [EnvironmentController::class, 'update'])
    ->name('environment.install');

