<?php

use App\Http\Controllers\CRM\Dashboard\DashboardController;

Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('deal-overview', [DashboardController::class, 'dealOverView'])->name('deal.overview');
