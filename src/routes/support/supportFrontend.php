<?php

use App\Http\Controllers\CRM\Deal\DealController;
use App\Http\Controllers\CRM\Frontend\FrontendController;
use Illuminate\Support\Facades\Route;
// Deal pipeline view

Route::get('deal/pipeline-view/{pipeline_id?}', [DealController::class, 'showPipelineView'])
    ->name('deal.pipeline_view')->middleware('can:view_deals');

Route::get('/deals/pipeline/view', [FrontendController::class, 'dealsPipelineView'])
    ->name('deals_pipeline.page')->middleware('can:view_deals');

Route::get('/deal/{id}/details', [FrontendController::class, 'dealDetails'])
    ->name('deal_details.page')->middleware('can:view_deals');
