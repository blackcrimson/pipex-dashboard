<?php

use App\Http\Controllers\CRM\Contact\OrganizationController;
use App\Http\Controllers\CRM\Contact\PersonController;
use App\Http\Controllers\CRM\Deal\DealController;
use App\Http\Controllers\CRM\Frontend\FrontendController;
	use App\Http\Controllers\CRM\Setting\EmailDeliveryCheckController;
	use App\Http\Controllers\CRM\Stage\DefaultStageController;
use App\Http\Controllers\CRM\Tags\TagController;
use App\Http\Controllers\CRM\AdditionalController;
use App\Http\Controllers\CRM\User\AppUserController;
use App\Http\Controllers\CRM\Setting\SettingsController;
use App\Http\Controllers\CRM\Contact\PhoneEmailTypeController;
use App\Http\Controllers\CRM\Proposal\ProposalController;

Route::get('general-settings', [SettingsController::class, 'index'])
    ->name('settings.general-settings');

Route::get('check-mail-delivery-setting', [EmailDeliveryCheckController::class, 'isExists'])
	->name('check_mail_delivery_setting');

Route::get('phone/email/type', [PhoneEmailTypeController::class, 'index'])
    ->name('phone_email.type');

Route::post('activities/done/{activity}', [AdditionalController::class, 'activitiesDone'])
    ->name('activities.done');

Route::post('note/update/{note}', [AdditionalController::class, 'noteUpdate'])
    ->name('activities.update-note');
Route::delete('note/delete/{note}', [AdditionalController::class, 'noteDestroy'])
    ->name('activities.delete-note');
Route::get('file/download/{file}', [AdditionalController::class, 'fileDownload']);

// All User

Route::get('auth/users', [AppUserController::class, 'crmAuthUsers'])->name('crm.auth_user');

// User Status
Route::get('user/statuses', [AdditionalController::class, 'statusesUser'])->name('statuses-user');
Route::get('users', [AppUserController::class, 'index']);
Route::get('user-social-links', [AppUserController::class, 'userSocialLink']);
Route::patch('user-social-links/update/{userId}', [AppUserController::class, 'userSocialLinkUpdate']);

// Custom Filed

Route::get('custom-field', [AdditionalController::class, 'customFieldSearch'])
    ->name('custom_field');

// Default Stages

Route::resource('stages-default', DefaultStageController::class);

// Deal Value

Route::get('deal-value', [DealController::class, 'getDealValue'])
    ->name('deal.value');


Route::get('activities/calendar/view', [FrontendController::class, 'activityCalendarView'])
    ->name('activities.calendar')->middleware('can:view_activities');


// Tags

Route::resource('tags', TagController::class)
    ->except('create', 'edit');


//Person Tags

Route::post('persons/tags/{person}', [PersonController::class, 'attachTag'])
    ->name('persons.attach-tag');
Route::put('persons/tags/{person}', [PersonController::class, 'detachTag'])
    ->name('persons.detach-tag');

// organizations Tags
Route::post(
    'organizations/tags/{organization}',
    [OrganizationController::class, 'attachTag']
)->name('organizations.attach-tag');
Route::put(
    'organizations/tags/{organization}',
    [OrganizationController::class, 'detachTag']
)->name('organizations.detach-tag');


// Deal tags

Route::post(
    'deal/tags/{deal}',
    [DealController::class, 'attachTag']
)->name('deal.attach-tag');

Route::put(
    'deal/tags/{deal}',
    [DealController::class, 'detachTag']
)->name('deal.detach-tag');
Route::post(
    'proposals/tags/{proposal}',
    [ProposalController::class, 'attachTag']
)->name('proposals.tag-attach');

Route::put(
    'proposals/tags/{proposal}',
    [ProposalController::class, 'detachTag']
)->name('proposals.tag-detach');

// Cache Clear

Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('optimize:clear');

    return redirect('/');
});
