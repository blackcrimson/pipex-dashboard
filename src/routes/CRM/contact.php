<?php

    /**
     * This route file contains all
     * of @var App\Models\CRM\Person\Person
     * and @var \App\Models\CRM\Organization\Organization
     * related routes.
     */

    use App\Http\Controllers\CRM\Contact\ContactTypeController;
    use App\Http\Controllers\CRM\Contact\OrganizationController;
    use App\Http\Controllers\CRM\Contact\OrganizationFollowerController;
    use App\Http\Controllers\CRM\Contact\PersonController;
    use App\Http\Controllers\CRM\Contact\PhoneEmailTypeController;

// Routes for Persons

    Route::resource('persons', PersonController::class);

    Route::post('person-import', [PersonController::class, 'importPerson'])
        ->name('person.import');

    Route::post(
        'persons/profile-picture/{person}',
        [PersonController::class, 'profilePicture']
    )->name('persons.upload-profile-picture-of');

    Route::get('person/navigation-change/{id}', [PersonController::class, 'personNavigationChange'])->name('persons.quick-view');


    Route::post(
        'persons/followers/{person}',
        [PersonController::class, 'personFollower']
    )->name('persons.sync-followers');

    Route::post(
        'persons/contact/sync/{person}',
        [PersonController::class, 'personContactSync']
    )->name('persons.sync-contact');

    Route::post(
        'persons/organizations/sync/{person}',
        [PersonController::class, 'organizationJobTitleSync']
    )->name('persons.sync-organizations');

    Route::get(
        'person/{person}/note',
        [PersonController::class, 'personNotes']
    )->name('person.note-view');

    Route::get(
        'person/{person}/file',
        [PersonController::class, 'personFiles']
    )->name('person.view-file');
    Route::get(
        'person/activities/{person}',
        [PersonController::class, 'personActivities']
    )->name('persons.view-activities');

    Route::post(
        'person/activities/sync/{person}',
        [PersonController::class, 'personActivitiesSync']
    )->name('person.sync-activities');

    Route::post(
        'person/note/sync/{person}',
        [PersonController::class, 'personNoteSync']
    )->name('person.sync-note');

    Route::post(
        'person/file/sync/{person}',
        [PersonController::class, 'personFileSync']
    )->name('person.sync-file');

    Route::get('persons/{person}/followers', [PersonController::class, 'personFollowers'])
        ->name('person_followers');

// Routes for Organizations

    Route::resource('organizations', OrganizationController::class);

    Route::post('organization-import', [OrganizationController::class, 'importOrganization'])
        ->name('organization.import');

    Route::post(
        'organizations/profile-picture/{organization}',
        [OrganizationController::class, 'profilePicture']
    )->name('organizations.upload-profile-picture-of');

    Route::get(
        'organization/{organization}/note',
        [OrganizationController::class, 'orgNotes']
    )->name('organization.note-view');

    Route::get(
        'organization/{organization}/file',
        [OrganizationController::class, 'orgFiles']
    )->name('organization.view-file');
    Route::get('navigation-change/{id}', [OrganizationController::class, 'navigationChange'])->name('organization.quick-view');


    Route::post(
        'organizations/sync/{organization}',
        [OrganizationController::class, 'personJobTitleSync']
    )->name('organizations.sync-person');


    Route::get(
        'phone/email/type',
        [PhoneEmailTypeController::class, 'index']
    )->name('phone_email.viw-types');

    Route::get(
        'organization/activities/{organization}',
        [OrganizationController::class, 'organizationActivities']
    )->name('organizations.view-activities');

    Route::post(
        'organization/activities/sync/{organization}',
        [OrganizationController::class, 'organizationActivitiesSync']
    )->name('organization.sync-activities');

    Route::post(
        'organization/note/sync/{organization}',
        [OrganizationController::class, 'organizationNoteSync']
    )->name('organization.sync-note');

    Route::post(
        'organization/file/sync/{organization}',
        [OrganizationController::class, 'organizationFileSync']
    )->name('organization.sync-file');

    Route::post('organizations/followers/{organization}', [OrganizationFollowerController::class, 'organizationFollower'])
            ->name('organizations.sync-follower');

    Route::get('organizations/{organization}/followers', [OrganizationFollowerController::class, 'organizationFollowers'])
          ->name('organization_followers');

// Routes for Contact Types

    Route::resource('contact/types', ContactTypeController::class)
        ->except('create', 'edit');
