<?php

/**
 * This route file contains all
 * related routes
 */

use App\Http\Controllers\CRM\Template\TemplateController;

Route::resource('/templates', TemplateController::class);
