<?php

use App\Http\Controllers\CRM\Report\DealReportController;
use App\Http\Controllers\CRM\Report\PipelineReportController;
use App\Http\Controllers\CRM\Report\ProposalReportController;

Route::group(['prefix' => 'reports'], function (){

    Route::get('proposals/chart',[ProposalReportController::class,'chart'])
        ->name('proposals.view-chart');

    Route::get('proposals/data-table',[ProposalReportController::class,'dataTable'])
        ->name('proposals.view-data-table');

    Route::get('proposal-details', [ProposalReportController::class, 'proposalReportDetails'])
        ->name('proposal-details');

    //Pipeline report
    Route::get('pipeline/chart',[PipelineReportController::class,'chart'])
    ->name('pipeline.view-chart');
    Route::get('pipeline/data-table',[PipelineReportController::class,'dataTable'])
    ->name('pipeline.view-data-table');
    Route::get('pipeline-details',[DealReportController::class,'pipelineReportDetails'])
    ->name('pipeline-details');
});

