<?php

use App\Models\Core\Status;
use App\Models\CRM\Deal\Deal;
use Faker\Generator as Faker;
use App\Models\Core\Auth\User;
use App\Models\CRM\Stage\Stage;
use App\Models\CRM\Deal\LostReason;
use App\Models\CRM\Pipeline\Pipeline;

$factory->define(Deal::class, function (Faker $faker) {
    $pipeline_id = $this->faker->randomElement(
        Pipeline::query()->pluck('id')->toArray()
    );

    $stage_id = $this->faker->randomElement(
        Stage::where('pipeline_id', $pipeline_id)
            ->pluck('id')
            ->toArray()
    );

    return [
        'title' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
        'value' => $this->faker->numberBetween(10000, 100000),
        'pipeline_id' => $pipeline_id,
        'stage_id' => $stage_id,
        'lost_reason_id' => $this->faker->randomElement(LostReason::query()->get('id')),
        'status_id' => $this->faker->randomElement(Status::whereType('deal')->get('id')),
        'created_by' => $this->faker->randomElement(User::query()->get('id')),
        'owner_id' => $this->faker->randomElement(User::query()->get('id')),
        'contextable_type' => $faker->randomElement(['App\Models\CRM\Person\Person', 'App\Models\CRM\Organization\Organization']),
        'contextable_id' => $faker->numberBetween(1, 15),
        'expired_at' => $this->faker->randomElement([null, $this->faker->dateTime]),
        'created_at' => $this->faker->dateTimeBetween('-1 month', 'now'),
    ];
});
